<?php

/**
 * @file
 * uw_vocab_feds_event_location.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function uw_vocab_feds_event_location_taxonomy_default_vocabularies() {
  return array(
    'feds_event_location' => array(
      'name' => 'Feds Event Location',
      'machine_name' => 'feds_event_location',
      'description' => 'Select the option most accurate to your event location',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
