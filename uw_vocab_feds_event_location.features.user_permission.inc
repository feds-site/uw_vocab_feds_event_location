<?php

/**
 * @file
 * uw_vocab_feds_event_location.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_vocab_feds_event_location_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'define view for terms in event_location'.
  $permissions['define view for terms in event_location'] = array(
    'name' => 'define view for terms in event_location',
    'roles' => array(),
    'module' => 'tvi',
  );

  // Exported permission: 'define view for vocabulary event_location'.
  $permissions['define view for vocabulary event_location'] = array(
    'name' => 'define view for vocabulary event_location',
    'roles' => array(),
    'module' => 'tvi',
  );

  // Exported permission: 'delete terms in event_location'.
  $permissions['delete terms in event_location'] = array(
    'name' => 'delete terms in event_location',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in event_location'.
  $permissions['edit terms in event_location'] = array(
    'name' => 'edit terms in event_location',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  return $permissions;
}
