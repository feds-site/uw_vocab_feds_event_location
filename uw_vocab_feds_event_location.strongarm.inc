<?php

/**
 * @file
 * uw_vocab_feds_event_location.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uw_vocab_feds_event_location_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'metatag_enable_taxonomy_term__event_location';
  $strongarm->value = TRUE;
  $export['metatag_enable_taxonomy_term__event_location'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_taxonomy_term_feds_event_location_pattern';
  $strongarm->value = '';
  $export['pathauto_taxonomy_term_feds_event_location_pattern'] = $strongarm;

  return $export;
}
