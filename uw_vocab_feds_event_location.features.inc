<?php

/**
 * @file
 * uw_vocab_feds_event_location.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_vocab_feds_event_location_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
