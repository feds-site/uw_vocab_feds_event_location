<?php

/**
 * @file
 * uw_vocab_feds_event_location.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function uw_vocab_feds_event_location_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'On Campus - Satellite Campus',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 2,
    'uuid' => '20229821-e4f9-468c-a727-0a9560ff94ce',
    'vocabulary_machine_name' => 'feds_event_location',
    'path' => array(
      'pathauto' => 0,
    ),
    'url_alias' => array(),
  );
  $terms[] = array(
    'name' => 'On Campus - Waterloo (Main Campus)',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 1,
    'uuid' => '6f6258d4-fc43-4025-b55c-1ef4b0a68f00',
    'vocabulary_machine_name' => 'feds_event_location',
    'path' => array(
      'pathauto' => 0,
    ),
    'url_alias' => array(),
  );
  $terms[] = array(
    'name' => 'Off Campus',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 3,
    'uuid' => 'b7ab676c-e4ce-40d3-8d90-68316c985a0a',
    'vocabulary_machine_name' => 'feds_event_location',
    'path' => array(
      'pathauto' => 0,
    ),
    'url_alias' => array(),
  );
  $terms[] = array(
    'name' => 'Student Life Centre (SLC)',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'c3eaf391-46db-4ead-89d7-3b70a9f2df39',
    'vocabulary_machine_name' => 'feds_event_location',
    'path' => array(
      'pathauto' => 0,
    ),
    'url_alias' => array(),
  );
  return $terms;
}
